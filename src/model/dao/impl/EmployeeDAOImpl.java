package model.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.AuTechConnection;
import model.dao.EmployeeDAO;
import model.obj.Employee;
import model.obj.Reimbursement;
import model.obj.ReimbursementStatus;

public class EmployeeDAOImpl implements EmployeeDAO {

	@Override
	public Employee login(String email, String password) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("SELECT * FROM employees WHERE email = ? AND password = ?;");
			
			ps.setString(1, email);
			ps.setString(2, password);
			
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return new Employee(
					rs.getInt("id"),
					rs.getString("first_name"),
					rs.getString("last_name"),
					rs.getString("position"),
					rs.getString("email"),
					rs.getString("phone"),
					rs.getString("password"),
					rs.getString("avatar_path"),
					rs.getBoolean("is_manager"),
					rs.getInt("manager_id")
				);		
			} else {
				return null;
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
			return null;
		} finally {
			atc.closeConnection();
		}
	}

	/**
	 * TODO this should be moved to managerDAO.
	 */
	@Override
	public boolean registerEmployee(Employee e) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("INSERT INTO employees "
					+ "(first_name, last_name, position, email, phone, password, avatar_path, is_manager, manager_id) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);");
			
			ps.setString(1, e.getFirstName());
			ps.setString(2, e.getLastName());
			ps.setString(3, e.getPosition());
			ps.setString(4, e.getEmail());
			ps.setString(5, e.getPhoneNumber());
			ps.setString(6, e.getPassword());
			ps.setString(7, e.getAvatarPath());
			ps.setBoolean(8, e.isManager());
			ps.setInt(9, e.getManagerId());
			
			ps.execute();
			return true;
		} catch (SQLException e1) {
			e1.printStackTrace();
			return false;
		} finally {
			atc.closeConnection();
		}
	}

	@Override
	public Employee getEmployee(int eid) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("SELECT * FROM employees WHERE id = ?;");
			
			ps.setInt(1, eid);
			
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return new Employee(
					rs.getInt("id"),
					rs.getString("first_name"),
					rs.getString("last_name"),
					rs.getString("position"),
					rs.getString("email"),
					rs.getString("phone"),
					rs.getString("password"),
					rs.getString("avatar_path"),
					rs.getBoolean("is_manager"),
					rs.getInt("manager_id")
				);		
			} else {
				return null;
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
			return null;
		} finally {
			atc.closeConnection();
		}
	}

	@Override
	public boolean updateEmployee(Employee e) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("UPDATE employees SET "
					+ "(first_name, last_name, email, phone) = "
					+ "(?, ?, ?, ?) WHERE id = ?;");
			
			ps.setString(1, e.getFirstName());
			ps.setString(2, e.getLastName());
			ps.setString(3, e.getEmail());
			ps.setString(4, e.getPhoneNumber());
			ps.setInt(5, e.getId());
			
			ps.executeUpdate();
			return true;
		} catch (SQLException e1) {
			e1.printStackTrace();
			return false;
		} finally {
			atc.closeConnection();
		}
	}

	@Override
	public boolean submitReimbursementRequest(Reimbursement r) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("INSERT INTO reimbursements "
					+ "(title, author, description, image_path, amount) "
					+ "VALUES (?, ?, ?, ?, ?);");
			
			ps.setString(1, r.getTitle());
			ps.setInt(2, r.getAuthor());
			ps.setString(3, r.getDescription());
			ps.setString(4, r.getImageLink());
			ps.setDouble(5, r.getAmount());
			
			ps.execute();
			return true;
		} catch (SQLException e1) {
			e1.printStackTrace();
			return false;
		} finally {
			atc.closeConnection();
		}
	}
	
	@Override
	public int getOwnerId(int rid) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("SELECT author FROM reimbursements WHERE id = ?;");
			
			ps.setInt(1, rid);
			
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return rs.getInt("author");
			} else {
				return -1;
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
			return -1;
		} finally {
			atc.closeConnection();
		}
	}
	
	@Override
	public Reimbursement viewOneReimbursementRequest(int rid) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("SELECT * FROM reimbursements WHERE id = ?;");
			
			ps.setInt(1, rid);
			
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return new Reimbursement(
					rs.getInt("id"),
					rs.getInt("author"),
					rs.getString("title"),
					rs.getString("description"),
					rs.getString("image_path"),
					ReimbursementStatus.valueOf(rs.getString("status").toUpperCase()),
					rs.getInt("amount")
				);
			} else {
				return null;
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
			return null;
		} finally {
			atc.closeConnection();
		}
	}

	@Override
	public List<Reimbursement> viewAllReimbursementRequests(int eid) {
		AuTechConnection atc = initConnection();
			
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("SELECT * FROM reimbursements WHERE author = ?;");
			
			ps.setInt(1, eid);
			
			ResultSet rs = ps.executeQuery();
			List<Reimbursement> reimbursements = new ArrayList<>();
			while(rs.next()) {
				Reimbursement r = new Reimbursement(
					rs.getInt("id"),
					rs.getInt("author"),
					rs.getString("title"),
					rs.getString("description"),
					rs.getString("image_path"),
					ReimbursementStatus.valueOf(rs.getString("status").toUpperCase()),
					rs.getInt("amount")
				);
				reimbursements.add(r);
			}
			
			return reimbursements;
		} catch (SQLException e1) {
			e1.printStackTrace();
			return null;
		} finally {
			atc.closeConnection();
		}
	}
	
	private AuTechConnection initConnection() {
		return new AuTechConnection(System.getenv("at_db_url"), System.getenv("at_db_user"), System.getenv("at_db_pass"));
	}
}
