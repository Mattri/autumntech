package test.main.webapp.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import main.webapp.AuthFilter;

class AuthFilterTest {
	
	@InjectMocks
	private static AuthFilter af;
	
	@Mock
	private HttpServletRequest sReq;
	
	@Mock
	private HttpServletResponse sRes;
	
	@Mock
	private HttpSession sess;
	
	@Mock
	private FilterChain fc;
	
	@BeforeAll
	public static void init() {
		af = new AuthFilter();
	}
	
	@BeforeEach
	public void openMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void testAuthSuccess() {
		Mockito.when(this.sReq.getSession(false)).thenReturn(sess);
		
		try {
			af.doFilter(this.sReq, this.sRes, this.fc);
			Mockito.verify(this.fc).doFilter(this.sReq, this.sRes);
		} catch (IOException | ServletException e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}
	
	@Test
	void testAuthFailure() {
		Mockito.when(this.sReq.getSession(false)).thenReturn(null);
		
		try {
			af.doFilter(this.sReq, this.sRes, this.fc);
			Mockito.verify(this.sRes).sendRedirect("/autumntech");
		} catch (IOException | ServletException e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}
}
