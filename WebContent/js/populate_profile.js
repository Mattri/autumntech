let requestedUserId = document.URL.split('/');
requestedUserId = requestedUserId[requestedUserId.indexOf("user") + 1];

let profileRequest = new XMLHttpRequest();
profileRequest.open("GET", "/autumntech/app/getuser/" + requestedUserId);

profileRequest.onload = (e) => {
  let employee = JSON.parse(profileRequest.responseText);

  document.getElementById("profile_name").innerText = employee.firstName + ' ' + employee.lastName;
  document.getElementById("profile_position").innerText = employee.position;
  document.getElementById("profile_email").innerText = employee.email;
  document.getElementById("profile_phone").innerText = employee.phoneNumber;
};

profileRequest.send();

//

let reimbursementsRequest = new XMLHttpRequest();
reimbursementsRequest.open("GET", "/autumntech/app/getreimbursements/" + requestedUserId);

reimbursementsRequest.onload = (e) => {
  let rList = JSON.parse(reimbursementsRequest.responseText);
  console.log(rList);
  
  let rbsmtContainer = document.getElementById("reimbursement_list");
  for(let reimbursement of rList) {
    rbsmtContainer.insertBefore(createReimbursementBlock(reimbursement), null);
  }
};

function createReimbursementBlock(reimbursement) {
  let container = document.createElement("li");
  container.setAttribute("class", "reimbursement_container");
  
  let link = document.createElement("a");
  link.setAttribute("class", "reimbursement_link");
  link.setAttribute("href", "/autumntech/app/rbsmt/" + reimbursement.id + "/");

  let title = document.createElement("h3");
  let amount = document.createElement("h4");
  let status = document.createElement("h4");

  title.innerText = reimbursement.title;
  amount.innerText = "$" + reimbursement.amount.toFixed(2);
  status.innerText = reimbursement.status;

  link.insertBefore(title, null);
  link.insertBefore(amount, null);
  link.insertBefore(status, null);
  
  container.insertBefore(link, null);
  return container;
}

reimbursementsRequest.send();

//

let editRequest = new XMLHttpRequest();
editRequest.open("GET", "/autumntech/app/isuserowner/" + requestedUserId);

editRequest.onload = (e) => {
  let isUserOwner = JSON.parse(editRequest.responseText);
  
  if(isUserOwner) {
    let editLink = document.createElement("a");
    editLink.setAttribute("href", "/autumntech/app/user_edit/" + requestedUserId + "/");

    let editButton = document.createElement("button");
    editButton.innerText = "Edit Profile";

    editLink.insertBefore(editButton, null);

    document.getElementById("profile_edit_container").insertBefore(editLink, null);
  }
};

editRequest.send();