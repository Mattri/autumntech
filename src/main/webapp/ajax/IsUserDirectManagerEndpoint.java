package main.webapp.ajax;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.webapp.URLChecker;
import model.dao.impl.ManagerDAOImpl;
import model.obj.Reimbursement;
import model.obj.ReimbursementStatus;
import model.service.ManagerService;

/**
 * Servlet implementation class IsUserDirectManagerEndpoint
 */
@WebServlet("/IsUserDirectManagerEndpoint")
public class IsUserDirectManagerEndpoint extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IsUserDirectManagerEndpoint() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO rigorous testing.
		int reimbursementId = 0;
		String path = request.getPathInfo();
		if(!URLChecker.validateSingleParameterPath(path)) {
			return;
		}
		
		try {
			reimbursementId = Integer.parseInt(path.substring(path.indexOf('/') + 1));
		} catch(NumberFormatException nfe) {
			return;
		}
		
		int managerId = Integer.parseInt(request.getSession().getAttribute("id").toString());
		List<Reimbursement> rList = new ManagerService().getAllSubordinateRequests(managerId, new ManagerDAOImpl());
		boolean isRequestPendingAndValid = false;
		for(Reimbursement r : rList) {
			if(r.getId() == reimbursementId && r.getStatus() == ReimbursementStatus.PENDING) {
				isRequestPendingAndValid = true;
				break;
			}
		}
				
		response.setContentType("text/html;charset=UTF-8");
		if(isRequestPendingAndValid) {
			response.getWriter().write("true");
		} else {
			response.getWriter().write("false");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
