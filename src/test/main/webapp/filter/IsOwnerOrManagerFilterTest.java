package test.main.webapp.filter;


import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import main.webapp.IsOwnerOrManagerFilter;

class IsOwnerOrManagerFilterTest {

	@InjectMocks
	private static IsOwnerOrManagerFilter iomf;
	
	@Mock
	private HttpServletRequest sReq;
	
	@Mock
	private HttpServletResponse sRes;
	
	@Mock
	private HttpSession sess;
	
	@Mock
	private FilterChain fc;
	
	@BeforeAll
	public static void init() {
		iomf = new IsOwnerOrManagerFilter();
	}
	
	@BeforeEach
	public void openMocks() {
		MockitoAnnotations.openMocks(this);
	}


	@Test
	void testIsOwnerSuccess() {
		int id = 4;
		Mockito.when(sReq.getPathInfo()).thenReturn("/" + id);
		Mockito.when(sReq.getSession(false)).thenReturn(sess);
		Mockito.when(sess.getAttribute("id")).thenReturn(id);
		Mockito.when(sess.getAttribute("isManager")).thenReturn("false");
		
		try {
			iomf.doFilter(sReq, sRes, fc);
			Mockito.verify(fc).doFilter(sReq, sRes);
		} catch (IOException | ServletException e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}
	
	@Test
	void testIsManagerSuccess() {
		int id = 4;
		Mockito.when(sReq.getPathInfo()).thenReturn("/" + id);
		Mockito.when(sReq.getSession(false)).thenReturn(sess);
		Mockito.when(sess.getAttribute("id")).thenReturn(id + 1);
		Mockito.when(sess.getAttribute("isManager")).thenReturn("true");
		
		try {
			iomf.doFilter(sReq, sRes, fc);
			Mockito.verify(fc).doFilter(sReq, sRes);
		} catch (IOException | ServletException e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}
	
	@Test
	void testIsOwnerOrManagerFailure() {
		int id = 4;
		Mockito.when(sReq.getPathInfo()).thenReturn("/" + id);
		Mockito.when(sReq.getSession(false)).thenReturn(sess);
		Mockito.when(sess.getAttribute("id")).thenReturn(id + 1);
		Mockito.when(sess.getAttribute("isManager")).thenReturn("false");
		
		try {
			iomf.doFilter(sReq, sRes, fc);
			Mockito.verify(sRes).sendRedirect("/autumntech/403.html");
		} catch (IOException | ServletException e) {
			e.printStackTrace();
			Assertions.fail();
		}

	}
}
