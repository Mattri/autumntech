let requestedReimbursementId = document.URL.split('/');
requestedReimbursementId = requestedReimbursementId[requestedReimbursementId.indexOf("rbsmt") + 1];

let superiorRequest = new XMLHttpRequest();
superiorRequest.open("GET", "/autumntech/app/isuserdirectmanager/" + requestedReimbursementId);

superiorRequest.onload = (e) => {
  console.log(superiorRequest.responseText);
  let isSuperior = JSON.parse(superiorRequest.responseText);

  if(isSuperior) {
    let decisionContainer = document.getElementById("decision_container");
    
    let header = document.createElement("h1");
    header.setAttribute("id", "decision_header");
    header.innerText = "You are the manager. Would you like to approve or deny this request?";

    let approveForm = document.createElement("form");
    approveForm.setAttribute("method", "POST");
    approveForm.setAttribute("action", "../../approve_rbsmt/" + requestedReimbursementId);
    let approveButton = document.createElement("button");
    approveButton.innerText = "Approve Reimbursement";
    approveForm.insertBefore(approveButton, null);

    let denyForm = document.createElement("form");
    denyForm.setAttribute("method", "POST");
    denyForm.setAttribute("action", "../../deny_rbsmt/" + requestedReimbursementId);
    let denyButton = document.createElement("button");
    denyButton.innerText = "Deny Reimbursement";
    denyForm.insertBefore(denyButton, null);

    decisionContainer.insertBefore(header, null);
    decisionContainer.insertBefore(approveForm, null);
    decisionContainer.insertBefore(denyForm, null);
  }
};

superiorRequest.send();