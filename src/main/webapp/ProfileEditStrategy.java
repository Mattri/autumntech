package main.webapp;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.impl.EmployeeDAOImpl;
import model.obj.Employee;
import model.service.EmployeeService;

/**
 * This POST strategy edits a user's information if all information provided is valid.
 * @author Matt Imel
 */
public class ProfileEditStrategy implements MainPostHelperStrategy {

	@Override
	public void executePost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		final String firstName = request.getParameter("firstName");
		final String lastName = request.getParameter("lastName");
		final String email = request.getParameter("email");
		final String phone = request.getParameter("phone");
		
		int userId = Integer.parseInt(request.getSession().getAttribute("id").toString());
		boolean success = new EmployeeService().updateEmployee(
			new Employee(userId, firstName, lastName, null, email, phone, null, null, false, -1),
			new EmployeeDAOImpl()
		);
		
		if(success) {
			response.sendRedirect("/autumntech/app/user/" + userId + "/");
		} else {
			// TODO send response indicating failure.
			response.sendRedirect("/autumntech/app/user/" + userId + "/");
		}
	}
}
