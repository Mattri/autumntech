package main.webapp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import model.dao.impl.EmployeeDAOImpl;
import model.obj.Reimbursement;
import model.obj.ReimbursementStatus;
import model.service.EmployeeService;

/**
 * This POST strategy submits a reimbursement request to the database. 
 * @author Matt Imel
 */
public class ReimbursementSubmissionStrategy implements MainPostHelperStrategy {

	@Override
	public void executePost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String title = request.getParameter("title");
		String amount = request.getParameter("amount");
		String desc = request.getParameter("desc");
		String path = "";
		
		int authorId = Integer.parseInt(request.getSession().getAttribute("id").toString());
		
		if(ServletFileUpload.isMultipartContent(request)) {
			try {
				List<FileItem> input = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				for(FileItem entry : input) {
					if(entry.isFormField()) {
						switch(entry.getFieldName()) {
							case "title":
								title = entry.getString();
								break;
							case "desc":
								desc = entry.getString();
								break;
							case "amount":
								amount = entry.getString();
								break;
						}
					} else {
						path = "/autumntech/assets/images/rbsmt/" + authorId + "/" + title.hashCode() + ".png";
						Path directory = Paths.get(Paths.get("").toAbsolutePath() + "\\WebContent\\assets\\images\\rbsmt\\" + authorId);
						
						if(!Files.exists(directory)) {
							Files.createDirectory(directory);
						}
						
						Path p = Paths.get(directory.toString() + title.hashCode() + ".png");
						Files.copy(entry.getInputStream(), p);
					}
				}
			} catch (FileUploadException e) {
				System.out.println("Cannot parse request.");
				e.printStackTrace();
			}
		}
		
		boolean success = new EmployeeService().submitReimbursementRequest(
			new Reimbursement(0, authorId, title, desc, path, ReimbursementStatus.PENDING, Double.parseDouble(amount)),
			new EmployeeDAOImpl()
		);
		
		if(success) {
			response.sendRedirect("/autumntech/app/");
		}
	}
}
