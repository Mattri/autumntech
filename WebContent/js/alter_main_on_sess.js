let sessionRequest = new XMLHttpRequest();
sessionRequest.open("GET", "/autumntech/app/getsession");

sessionRequest.onload = (e) => {
  let employee = JSON.parse(sessionRequest.responseText);

  console.log(employee);
  if(employee.isManager) {
    let optionsList = document.getElementById("options_list");

    // Add employee roster option.
    let allEmployeesOption = document.createElement("li");
    let allEmployeesLink = document.createElement("a");
    allEmployeesLink.innerText = "View Your Employees";
    allEmployeesLink.setAttribute("href", "/autumntech/app/team/" + employee.id + "/");
    allEmployeesOption.insertBefore(allEmployeesLink, null);
    optionsList.insertBefore(allEmployeesOption, null);

    // Add search option.
    let searchOption = document.createElement("li");
    let searchLink = document.createElement("a");
    searchLink.innerText = "Search Employees or Reimbursements";
    searchLink.setAttribute("href", "/autumntech/app/search/");
    searchOption.insertBefore(searchLink, null);
    optionsList.insertBefore(searchOption, null);
  }

  document.getElementById("header_profile_link").setAttribute("href", "/autumntech/app/user/" + employee.id + "/");
  document.getElementById("profile_link").setAttribute("href", "/autumntech/app/user/" + employee.id + "/");
};

sessionRequest.send();