package main.webapp.ajax;

import java.util.List;

public final class JSONArrayUtil {
	public static final String convertListToJSON(List<?> list) {
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		
		for(int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));
			
			if(i + 1 != list.size()) {
				sb.append(',');
			}
		}
		
		sb.append(']');
		return sb.toString();
	}
}
