package main.webapp;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This filter checks whether or not the user is the owner of the page (for example, if a user profile page belongs to the
 * logged-in user). If not, this denies the user access to the page.
 * @author Matt Imel
 */
@WebFilter("/IsOwnerFilter")
public class IsOwnerFilter implements Filter {

    /**
     * Default constructor. 
     */
    public IsOwnerFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		int requestedId = 0;
		String path = req.getPathInfo();
		if(!URLChecker.validateSingleParameterPath(path)) {
			res.sendRedirect("/autumntech/404.html");
		}
		
		try {
			requestedId = Integer.parseInt(path.replace("/", ""));
		} catch(NumberFormatException nfe) {
			return;
		}
				
		boolean isOwner = Integer.parseInt(req.getSession(false).getAttribute("id").toString()) == requestedId;
				
		if(isOwner) {
			chain.doFilter(request, response);
		} else {
			res.sendRedirect("/autumntech/403.html");
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
