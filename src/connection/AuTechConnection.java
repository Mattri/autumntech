package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * A connection object, used to connect to the AutumnTech database.
 * @author Matt Imel
 */
public class AuTechConnection {
	private Connection dbConn;
	
	public AuTechConnection(String url, String username, String password) {
		try {
			Class.forName("org.postgresql.Driver");
			this.dbConn = DriverManager.getConnection("jdbc:postgresql://" + url + "/autumntech", username, password);
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public Connection getConnection() {
		return this.dbConn;
	}
	
	public void closeConnection() {
		try {
			this.dbConn.close();
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		}
	}
}
