package main.webapp;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Classes that implement this interface must have logic pertinent to a POST request.
 * @author Matt Imel
 */
@FunctionalInterface
public interface MainPostHelperStrategy {
	void executePost(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
