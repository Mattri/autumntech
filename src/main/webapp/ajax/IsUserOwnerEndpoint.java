package main.webapp.ajax;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.webapp.URLChecker;

/**
 * Servlet implementation class IsUserOwnerEndpoint
 */
@WebServlet("/IsUserOwnerEndpoint")
public class IsUserOwnerEndpoint extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IsUserOwnerEndpoint() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int profileId = 0;
		String path = request.getPathInfo();
		if(!URLChecker.validateSingleParameterPath(path)) {
			return;
		}
		
		try {
			profileId = Integer.parseInt(path.substring(path.indexOf('/') + 1));
		} catch(NumberFormatException nfe) {
			return;
		}
		
		int sessionId = Integer.parseInt(request.getSession().getAttribute("id").toString());
		
		response.setContentType("text/html;charset=UTF-8");
		if(profileId == sessionId) {
			response.getWriter().write("true");
		} else {
			response.getWriter().write("false");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
