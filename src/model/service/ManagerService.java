package model.service;

import java.util.List;

import model.dao.ManagerDAO;
import model.dao.impl.EmployeeDAOImpl;
import model.obj.Employee;
import model.obj.Reimbursement;
import model.obj.ReimbursementStatus;

/**
 * A service layer containing actions only managers can do. 
 * @author Matt Imel
 *
 */
public class ManagerService {
	/**
	 * Approves a reimbursement request submitted by a direct subordinate of this manager.
	 * @param rid The id of the reimbursement request to approve.
	 * @param md The ManagerDAO implementation to use.
	 * @return True if the reimbursement was successfully approved; false otherwise.
	 */
	public boolean approveReimbursementRequest(int rid, ManagerDAO md, EmployeeService es) {
		Reimbursement r = es.viewOneReimbursementRequest(rid, new EmployeeDAOImpl());
		if(r != null && r.getStatus() == ReimbursementStatus.PENDING) {
			return md.approveReimbursementRequest(rid);
		} else {
			return false;
		}
	}
	
	/**
	 * Denies a reimbursement request submitted by a direct subordinate of this manager.
	 * @param rid The id of the reimbursement request to deny.
	 * @param md The ManagerDAO implementation to use.
	 * @return True if the reimbursement was successfully denied; false otherwise.
	 */
	public boolean denyReimbursementRequest(int rid, ManagerDAO md, EmployeeService es) {
		Reimbursement r = es.viewOneReimbursementRequest(rid, new EmployeeDAOImpl());
		if(r != null && r.getStatus() == ReimbursementStatus.PENDING) {
			return md.denyReimbursementRequest(rid);
		} else {
			return false;
		}
		
	}
	
	public List<Reimbursement> getAllSubordinateRequests(int mid, ManagerDAO md) {
		return md.getAllSubordinateRequests(mid);
	}
	
	public List<Employee> getAllEmployeesUnder(int mid, ManagerDAO md) {
		return md.getAllEmployeesUnder(mid);
	}
	
	public List<Employee> searchEmployeesByName(String nameQuery, ManagerDAO md) {
		return md.getEmployeesByName(nameQuery);
	}
	
	public List<Reimbursement> searchReimbursementsByTitle(String titleQuery, ManagerDAO md) {
		return md.getReimbursementsByTitle(titleQuery);
	}
}
