package model.service;

import java.util.List;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.RegexValidator;

import model.dao.EmployeeDAO;
import model.obj.Employee;
import model.obj.Reimbursement;

/**
 * A service layer for all employee methods which all employees can use,
 * regardless of managerial status.
 * @author Matt Imel
 *
 */
public class EmployeeService {
	
	public Employee login(String email, String password, EmployeeDAO ed) {
		return ed.login(email, password);
	}

	/**
	 * Gets the details of an employee in the database by employee id.
	 * @param eid The employee id to check.
	 * @param edi The employee DAO implementation to use
	 * @return An Employee object with details if the employee id is present in the database; null otherwise.
	 */
	public Employee getEmployee(int eid, EmployeeDAO ed) {
		return ed.getEmployee(eid);
	}
	
	public boolean updateEmployee(Employee e, EmployeeDAO ed) {
		EmailValidator ev = EmailValidator.getInstance();
		// Regex created by Don Johnston
		// https://regexlib.com/REDetails.aspx?regexp_id=607
		RegexValidator rv = new RegexValidator("^(?:\\([2-9]\\d{2}\\)\\ ?|[2-9]\\d{2}(?:\\-?|\\ ?))[2-9]\\d{2}[- ]?\\d{4}$");
		if(ev.isValid(e.getEmail()) && rv.isValid(e.getPhoneNumber())) {
			return ed.updateEmployee(e);
		} else {
			return false;
		}
	}
	
	public boolean submitReimbursementRequest(Reimbursement r, EmployeeDAO ed) {
		if(r.getTitle() != null && r.getDescription() != null && r.getAuthor() > 0 && r.getAmount() >= 0.01) {
			return ed.submitReimbursementRequest(r);
		} else {
			return false;
		}
	}
	
	public int getOwnerId(int rid, EmployeeDAO ed) {
		return ed.getOwnerId(rid);
	}
	
	public Reimbursement viewOneReimbursementRequest(int rid, EmployeeDAO ed) {
		return ed.viewOneReimbursementRequest(rid);
	}
	
	public List<Reimbursement> viewAllReimbursementRequests(int eid, EmployeeDAO ed) {
		return ed.viewAllReimbursementRequests(eid);
	}
}
