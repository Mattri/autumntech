package test.main.webapp.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import main.webapp.IsManagerFilter;

class IsManagerFilterTest {

	@InjectMocks
	private static IsManagerFilter imf;
	
	@Mock
	private HttpServletRequest sReq;
	
	@Mock
	private HttpServletResponse sRes;
	
	@Mock
	private FilterChain fc;
	
	@Mock
	private HttpSession sess;
	
	@BeforeAll
	public static void init() {
		imf = new IsManagerFilter();
	}
	
	@BeforeEach
	public void openMocks() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	void testIsManagerSuccess() {
		Mockito.when(sReq.getSession(false)).thenReturn(sess);
		Mockito.when(sess.getAttribute("isManager")).thenReturn("true");
		
		try {
			imf.doFilter(sReq, sRes, fc);
			Mockito.verify(fc).doFilter(sReq, sRes);
		} catch (IOException | ServletException e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}
	
	@Test
	void testIsManagerFailure() {
		Mockito.when(sReq.getSession(false)).thenReturn(sess);
		Mockito.when(sess.getAttribute("isManager")).thenReturn("false");
		
		try {
			imf.doFilter(sReq, sRes, fc);
			Mockito.verify(sRes).sendRedirect("/autumntech/403.html");
		} catch (IOException | ServletException e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}
}
