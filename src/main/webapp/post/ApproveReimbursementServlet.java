package main.webapp.post;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.impl.ManagerDAOImpl;
import model.service.EmployeeService;
import model.service.ManagerService;

/**
 * This POST servlet approves a given reimbursement request.
 */
@WebServlet("/ApproveReimbursementServlet")
public class ApproveReimbursementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int rId = 0;
		String path = request.getPathInfo();
		try {
			rId = Integer.parseInt(path.substring(path.indexOf('/') + 1));
		} catch(NumberFormatException nfe) {
			return;
		}
		
		boolean success = new ManagerService().approveReimbursementRequest(rId, new ManagerDAOImpl(), new EmployeeService());
		
		if(success) {
			response.sendRedirect("/autumntech/app/rbsmt/" + rId + "/");
		} else {
			response.sendRedirect("/autumntech/403.html");
		}
	}

}
