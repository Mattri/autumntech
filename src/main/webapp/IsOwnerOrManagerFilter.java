package main.webapp;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This filter checks to see if the page someone is trying to access is being accessed
 * by someone in a managerial position, or is the owner of the page.
 */
@WebFilter("/IsManagerFilter")
public class IsOwnerOrManagerFilter implements Filter {

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		int requestedId = 0;
		String path = req.getPathInfo();
		if(!URLChecker.validateSingleParameterPath(path)) {
			res.sendRedirect("/autumntech/404.html");
		}
		
		try {
			requestedId = Integer.parseInt(path.replace("/", ""));
		} catch(NumberFormatException nfe) {
			res.sendRedirect("/autumntech/404.html");
			return;
		}
		
		boolean isOwner = Integer.parseInt(req.getSession(false).getAttribute("id").toString()) == requestedId;
		boolean isManager = Boolean.parseBoolean(req.getSession(false).getAttribute("isManager").toString());

		if(isOwner || isManager) {
			chain.doFilter(request, response);
		} else {
			res.sendRedirect("/autumntech/403.html");
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
