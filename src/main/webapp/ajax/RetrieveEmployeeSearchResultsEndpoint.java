package main.webapp.ajax;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.webapp.URLChecker;
import model.dao.impl.ManagerDAOImpl;
import model.obj.Employee;
import model.service.ManagerService;

/**
 * Servlet implementation class RetrieveEmployeeSearchResultsEndpoint
 */
@WebServlet("/RetrieveEmployeeSearchResultsEndpoint")
public class RetrieveEmployeeSearchResultsEndpoint extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String path = request.getPathInfo();
		if(!URLChecker.validateSingleParameterPath(path)) {
			return;
		}
		String titleQuery = path.replace("/", "");
		
		List<Employee> e = new ManagerService().searchEmployeesByName(titleQuery, new ManagerDAOImpl());
		
		if(e != null) {
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(JSONArrayUtil.convertListToJSON(e));
		}
	}
}
