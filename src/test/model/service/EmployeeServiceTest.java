package test.model.service;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import model.dao.impl.EmployeeDAOImpl;
import model.obj.Employee;
import model.obj.Reimbursement;
import model.obj.ReimbursementStatus;
import model.service.EmployeeService;

class EmployeeServiceTest {
	@InjectMocks
	private static EmployeeService es;
	
	@Mock
	private EmployeeDAOImpl edi;
	
	private static Employee dummyEmployee;
	private static Reimbursement dummyReimbursement;
	
	@BeforeAll
	public static void init() {
		es = new EmployeeService();
		dummyEmployee = new Employee(
				44,
				"Test",
				"Employee",
				"CEO",
				"test@autech.com",
				"913-232-4231",
				"pass",
				"",
				false,
				55
		);
		
		dummyReimbursement = new Reimbursement(
				3,
				44,
				"Test Rbsmt",
				"This is a test reimbursement.",
				"",
				ReimbursementStatus.PENDING,
				30.00
		);
	}
	
	@BeforeEach
	public void openMocks() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	void testLoginSuccess() {
		String user = "test@autech.com";
		String pass = "pass";
		Mockito.when(this.edi.login(user, pass)).thenReturn(dummyEmployee);
		
		Employee e = es.login(user, pass, edi);
		
		Assertions.assertEquals(dummyEmployee.getFirstName(), e.getFirstName());
	}

	@Test
	void testGetEmployeeSuccess() {
		int employeeId = dummyEmployee.getId();
		Mockito.when(this.edi.getEmployee(employeeId)).thenReturn(dummyEmployee);
		
		Employee e = es.getEmployee(employeeId, edi);
		
		Assertions.assertEquals(dummyEmployee.getFirstName(), e.getFirstName());
	}
	
	@Test
	void testGetOneReimbursementSuccess() {
		Mockito.when(this.edi.viewOneReimbursementRequest(dummyReimbursement.getId()))
		.thenReturn(dummyReimbursement);
		
		Reimbursement r = es.viewOneReimbursementRequest(dummyReimbursement.getId(), edi);
		Assertions.assertEquals(dummyReimbursement.getAuthor(), r.getAuthor());
	}
	
	@Test
	void testGetAllReimbursementsSuccess() {
		int employeeId = 44;
		Mockito.when(this.edi.viewAllReimbursementRequests(employeeId)).thenReturn(
			Arrays.asList(new Reimbursement(
				1,
				44,
				"",
				"",
				"",
				ReimbursementStatus.PENDING,
				1.00
			), dummyReimbursement)
		);
		
		List<Reimbursement> rList = es.viewAllReimbursementRequests(employeeId, edi);
		Assertions.assertEquals(rList.get(0).getAmount(), 1.00);
	}
	
	@Test
	void testUpdateEmployeeSuccess() {
		Mockito.when(this.edi.updateEmployee(dummyEmployee)).thenReturn(true);
		
		boolean success = es.updateEmployee(dummyEmployee, edi);
		
		Assertions.assertEquals(true, success);
	}
	
	@Test
	void testUpdateEmployeeFailEmail() {
		Mockito.when(this.edi.updateEmployee(new Employee(
				44,
				"Test",
				"Employee",
				"CEO",
				"Bad Email",
				"913-232-4231",
				"pass",
				"",
				false,
				55
		))).thenReturn(true);
		
		boolean success = es.updateEmployee(dummyEmployee, edi);
		
		Assertions.assertEquals(false, success);
	}
	
	@Test
	void testUpdateEmployeeFailPhone() {
		Mockito.when(this.edi.updateEmployee(new Employee(
				44,
				"Test",
				"Employee",
				"CEO",
				"test@autech.com",
				"Bad Phone Number",
				"pass",
				"",
				false,
				55
		))).thenReturn(true);
		
		boolean success = es.updateEmployee(dummyEmployee, edi);
		
		Assertions.assertEquals(false, success);
	}
}
