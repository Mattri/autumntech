package main.webapp;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This POST strategy logs the user out.
 * @author Matt Imel.
 */
public class LogoutStrategy implements MainPostHelperStrategy {
	@Override
	public void executePost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession sess = request.getSession(false);
		if(sess != null) {
			sess.invalidate();
		}
		response.sendRedirect("/autumntech");
	}
}
