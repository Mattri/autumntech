package main.webapp.ajax;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.webapp.URLChecker;
import model.dao.impl.ManagerDAOImpl;
import model.obj.Employee;
import model.service.ManagerService;

/**
 * Servlet implementation class RetrieveManagerTeam
 */
@WebServlet("/RetrieveManagerTeam")
public class RetrieveManagerTeamEndpoint extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RetrieveManagerTeamEndpoint() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int managerId = 0;
		String path = request.getPathInfo();
		if(!URLChecker.validateSingleParameterPath(path)) {
			return;
		}
		
		try {
			managerId = Integer.parseInt(path.substring(path.indexOf('/') + 1));
		} catch(NumberFormatException nfe) {
			return;
		}
		
		List<Employee> eList = new ManagerService().getAllEmployeesUnder(managerId, new ManagerDAOImpl());
		
		if(eList != null) {
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(JSONArrayUtil.convertListToJSON(eList));
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
