let requestedUserId = document.URL.split('/');
requestedUserId = requestedUserId[requestedUserId.indexOf("rbsmt") + 1];

let reimbursementRequest = new XMLHttpRequest();
reimbursementRequest.open("GET", "/autumntech/app/get_1_reimbursement/" + requestedUserId);

reimbursementRequest.onload = (e) => {
  let reimbursement = JSON.parse(reimbursementRequest.responseText);

  let userLink = document.createElement("a");
  userLink.setAttribute("href", "/autumntech/app/user/" + reimbursement.author + "/");
  userLink.innerText = reimbursement.author;

  document.getElementById("reimbursement_title").innerText = reimbursement.title;
  document.getElementById("reimbursement_author").insertBefore(userLink, null);
  document.getElementById("reimbursement_desc").innerText = reimbursement.description;
  document.getElementById("reimbursement_cost").innerText = "$" + reimbursement.amount.toFixed(2);
  document.getElementById("reimbursement_status").innerText = reimbursement.status;
  document.getElementById("reimbursement_image").setAttribute("src", reimbursement.imageLink);
};

reimbursementRequest.send();
