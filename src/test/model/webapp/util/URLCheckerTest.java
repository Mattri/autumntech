package test.model.webapp.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import main.webapp.URLChecker;

class URLCheckerTest {

	@Test
	void successOneSlash() {
		Assertions.assertEquals(true, URLChecker.validateSingleParameterPath("/test"));
	}
	
	@Test
	void successTwoSlashes() {
		Assertions.assertEquals(true, URLChecker.validateSingleParameterPath("/test/"));
	}
	
	@Test
	void failTooManyParams() {
		Assertions.assertEquals(false, URLChecker.validateSingleParameterPath("/test/too/many"));
	}
	
	@Test
	void failNoParam() {
		Assertions.assertEquals(false, URLChecker.validateSingleParameterPath("/"));
	}
}
