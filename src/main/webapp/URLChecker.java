package main.webapp;

/**
 * This is a utility class meant to check the validity of URLs, with regards to this project.
 * @author Matt Imel
 */
public final class URLChecker {
	
	/**
	 * Checks that a URL has a single parameter at the end, and that it ends with a forward slash.
	 * @param path The URL to check.
	 * @return True if the URL abides by these conditions, or false otherwise.
	 */
	public static boolean validateSingleParameterPath(String path) {
		boolean hasOneSlash = path.indexOf('/') == path.lastIndexOf('/') && path.indexOf('/') != 1 && path.length() > 1;
		int numSlashes = 0;
		for(int i = 0; i < path.length(); i++) {
			if(path.charAt(i) == '/') {
				numSlashes += 1;
			}
		}
		boolean hasTwoSlashes = numSlashes == 2 && path.endsWith("/");
		return hasOneSlash || hasTwoSlashes;
	}
}
