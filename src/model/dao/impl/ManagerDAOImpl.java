package model.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.AuTechConnection;
import model.dao.ManagerDAO;
import model.obj.Employee;
import model.obj.Reimbursement;
import model.obj.ReimbursementStatus;

public class ManagerDAOImpl implements ManagerDAO {

	@Override
	public boolean approveReimbursementRequest(int rid) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("UPDATE reimbursements SET "
					+ "status = 'approved' "
					+ "WHERE id = ?;");
			
			ps.setInt(1, rid);
			
			ps.executeUpdate();
			return true;
		} catch (SQLException e1) {
			e1.printStackTrace();
			return false;
		} finally {
			atc.closeConnection();
		}
	}

	@Override
	public boolean denyReimbursementRequest(int rid) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("UPDATE reimbursements SET "
					+ "status = 'denied' "
					+ "WHERE id = ?;");
			
			ps.setInt(1, rid);
			
			ps.executeUpdate();
			return true;
		} catch (SQLException e1) {
			e1.printStackTrace();
			return false;
		} finally {
			atc.closeConnection();
		}
	}
	
	@Override
	public List<Employee> getAllEmployeesUnder(int mid) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("SELECT * FROM employees e "
					+ "WHERE e.manager_id = ?;");
			ps.setInt(1, mid);
			
			ResultSet rs = ps.executeQuery();
			List<Employee> employees = new ArrayList<>();
			while(rs.next()) {
				Employee e = new Employee(
					rs.getInt("id"),
					rs.getString("first_name"),
					rs.getString("last_name"),
					rs.getString("position"),
					rs.getString("email"),
					rs.getString("phone"),
					rs.getString("password"),
					rs.getString("avatar_path"),
					rs.getBoolean("is_manager"),
					rs.getInt("manager_id")
				);
				employees.add(e);
			}
			
			return employees;
		} catch (SQLException e1) {
			e1.printStackTrace();
			return null;
		} finally {
			atc.closeConnection();
		}
	}

	@Override
	public List<Reimbursement> getAllSubordinateRequests(int mid) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("SELECT * FROM reimbursements r "
					+ "RIGHT JOIN employees e ON r.author = e.id "
					+ "WHERE e.manager_id = ?;");
			ps.setInt(1, mid);
			
			ResultSet rs = ps.executeQuery();
			List<Reimbursement> reimbursements = new ArrayList<>();
			while(rs.next()) {
				Reimbursement r = new Reimbursement(
					rs.getInt("id"),
					rs.getInt("author"),
					rs.getString("title"),
					rs.getString("description"),
					rs.getString("image_path"),
					ReimbursementStatus.valueOf(rs.getString("status").toUpperCase()),
					rs.getInt("amount")
				);
				reimbursements.add(r);
			}
			
			return reimbursements;
		} catch (SQLException e1) {
			e1.printStackTrace();
			return null;
		} finally {
			atc.closeConnection();
		}
	}

	@Override
	public List<Employee> getEmployeesByName(String nameQuery) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("SELECT * FROM employees e "
					+ "WHERE CONCAT(e.first_name, ' ', e.last_name) ILIKE CONCAT('%', ?, '%')");
			ps.setString(1, nameQuery);
			
			ResultSet rs = ps.executeQuery();
			List<Employee> employees = new ArrayList<>();
			while(rs.next()) {
				Employee e = new Employee(
					rs.getInt("id"),
					rs.getString("first_name"),
					rs.getString("last_name"),
					rs.getString("position"),
					rs.getString("email"),
					rs.getString("phone"),
					rs.getString("password"),
					rs.getString("avatar_path"),
					rs.getBoolean("is_manager"),
					rs.getInt("manager_id")
				);
				employees.add(e);
			}
			
			return employees;
		} catch (SQLException e1) {
			e1.printStackTrace();
			return null;
		} finally {
			atc.closeConnection();
		}
	}
	
	@Override
	public List<Reimbursement> getReimbursementsByTitle(String titleQuery) {
		AuTechConnection atc = initConnection();
		
		try {
			PreparedStatement ps = atc.getConnection().prepareStatement("SELECT * FROM reimbursements r "
					+ "WHERE r.title ILIKE CONCAT('%', ?, '%');");
			ps.setString(1, titleQuery);
			
			ResultSet rs = ps.executeQuery();
			List<Reimbursement> reimbursements = new ArrayList<>();
			while(rs.next()) {
				Reimbursement r = new Reimbursement(
					rs.getInt("id"),
					rs.getInt("author"),
					rs.getString("title"),
					rs.getString("description"),
					rs.getString("image_path"),
					ReimbursementStatus.valueOf(rs.getString("status").toUpperCase()),
					rs.getInt("amount")
				);
				reimbursements.add(r);
			}
			
			return reimbursements;
		} catch (SQLException e1) {
			e1.printStackTrace();
			return null;
		} finally {
			atc.closeConnection();
		}
	}
	
	private AuTechConnection initConnection() {
		return new AuTechConnection(System.getenv("at_db_url"), System.getenv("at_db_user"), System.getenv("at_db_pass"));
	}
}
