INSERT INTO employees (
  first_name,
  last_name,
  "position",
  email,
  "password",
  phone
) VALUES (
  'Jan',
  'Febre',
  'CEO',
  'jen@autech.com',
  'password',
  '(913)-020-0423'
);

INSERT INTO employees (
  first_name,
  last_name,
  "position",
  email,
  "password",
  is_manager,
  phone
) VALUES (
  'Marc',
  'Abril',
  'CFO',
  'marc@autech.com',
  'pass',
  true,
  '(913)-020-3333'
);

UPDATE employees SET is_manager = true WHERE first_name = 'Marc';
UPDATE employees SET managerId = (
  SELECT id FROM employees WHERE first_name = 'Marc'
) WHERE first_name = 'Jan';