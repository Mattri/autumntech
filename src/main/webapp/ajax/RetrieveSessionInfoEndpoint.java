package main.webapp.ajax;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class RetrieveSessionInfoEndpoint
 */
@WebServlet("/RetrieveSessionInfoEndpoint")
public class RetrieveSessionInfoEndpoint extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sess = request.getSession(false);
		
		if(sess != null) {
			SessionInfo si = new SessionInfo(sess.getAttribute("id").toString(), sess.getAttribute("isManager").toString());
			
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(si.toString());
		}
	}
	
	private class SessionInfo {
		private String id;
		private String isManager;
		
		private SessionInfo(String id, String isManager) {
			this.id = id;
			this.isManager = isManager;
		}

		@Override
		public String toString() {
			return "{\"id\":" + id + ", \"isManager\":" + isManager + "}";
		}
	}
}
