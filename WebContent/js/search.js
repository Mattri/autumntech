document.getElementById("submit_search_query").addEventListener("click", (e) => {
  let employeeContainer = document.getElementById("employee_results");
  let rbsmtContainer = document.getElementById("reimbursement_results");
  employeeContainer.innerHTML = "";
  rbsmtContainer.innerHTML = "";

  if(document.getElementById("employee_radio").checked) {
    let searchEPromise = retrieveInfo("/autumntech/app/search_e/", document.getElementById("search_query").value);
    searchEPromise.then((employeeData) => {
      for(const employee of employeeData) {
        employeeContainer.insertBefore(createEmployeeElement(employee), null);
      }
    });
  } else {
    let searchRPromise = retrieveInfo("/autumntech/app/search_r/", document.getElementById("search_query").value);
    searchRPromise.then((rbsmtData) => {
      for(const rbsmt of rbsmtData) {
        rbsmtContainer.insertBefore(createReimbursementBlock(rbsmt), null);
      }
    });
  }
});

function retrieveInfo(url, query) {
  return new Promise((res, rej) => {
    let searchRequest = new XMLHttpRequest();
    searchRequest.open("GET", url + query);

    searchRequest.onload = (e) => {
      res(JSON.parse(searchRequest.responseText));
    };

    searchRequest.send();
  });
}

function createEmployeeElement(employee) {
  let container = document.createElement("li");
  container.setAttribute("class", "employee_container");

  let link = document.createElement("a");
  link.setAttribute("href", "/autumntech/app/user/" + employee.id + "/");
  link.setAttribute("class", "employee_link");

  let name = document.createElement("h3");
  name.innerText = employee.firstName + " " + employee.lastName;
  let position = document.createElement("h4");
  position.innerText = employee.position;

  link.insertBefore(name, null);
  link.insertBefore(position, null);

  container.insertBefore(link, null);
  return container;
}

function createReimbursementBlock(reimbursement) {
  let container = document.createElement("li");
  container.setAttribute("class", "reimbursement_container");
  
  let link = document.createElement("a");
  link.setAttribute("class", "reimbursement_link");
  link.setAttribute("href", "/autumntech/app/rbsmt/" + reimbursement.id + "/");

  let title = document.createElement("h3");
  let amount = document.createElement("h4");
  let status = document.createElement("h4");

  title.innerText = reimbursement.title;
  amount.innerText = "$" + reimbursement.amount.toFixed(2);
  status.innerText = reimbursement.status;

  link.insertBefore(title, null);
  link.insertBefore(amount, null);
  link.insertBefore(status, null);
  
  container.insertBefore(link, null);
  return container;
}