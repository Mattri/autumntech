# Expense Reimbursement System (AutumnTech)
This expense reimbursement system allows users to submit reimbursement requests, which managers are allowed to review and either approve or deny. Employees also have accounts, which they may edit. Managers can view all employees and their reimbursement requests.

## Employee Features
* Logging in and logging out
* Editing their profile
* Submitting a reimbursement request
* Viewing their reimbursement requests

## Manager Features
* All Employee features
* Viewing all employees and reimbursement requests
* Approving and denying reimbursement requests made by employees under them

## Technologies Used
* Java 8
* Servlets
* Maven
* Tomcat
* HTML5
* CSS3
* JavaScript
* AJAX
* JUnit
* Mockito

