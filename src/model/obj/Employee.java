package model.obj;

/**
 * An employee data object.
 * @author Matt Imel
 */
public class Employee {
	private int id;
	private String firstName;
	private String lastName;
	private String position;
	private String email;
	private String phoneNumber;
	private String password;
	private String avatarPath;
	private boolean isManager;
	private int managerId;
	
	public Employee(int id, String firstName, String lastName, String position, String email, String phoneNumber,
			String password, String avatarPath, boolean isManager, int managerId) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.position = position;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.password = password;
		this.avatarPath = avatarPath;
		this.isManager = isManager;
		this.managerId = managerId;
	}

	public String getPassword() {
		return password;
	}

	public String getAvatarPath() {
		return avatarPath;
	}

	public boolean isManager() {
		return isManager;
	}

	public int getManagerId() {
		return managerId;
	}

	public int getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPosition() {
		return position;
	}

	public String getEmail() {
		return email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPassword(String newPassword) {
		this.password = newPassword;
	}

	/**
	 * Returns a JSON-compliant representation of this object.
	 */
	@Override
	public String toString() {
		return "{\"id\":" + id + ", \"firstName\":\"" + firstName + "\", \"lastName\":\"" + lastName + "\", \"position\":\"" + position
				+ "\", \"email\":\"" + email + "\", \"phoneNumber\":\"" + phoneNumber + "\", \"avatarPath\":\""
				+ avatarPath + "\", \"isManager\":" + isManager + ", \"managerId\":" + managerId + "}";
	}
}
