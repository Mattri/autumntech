let requestedManagerId = document.URL.split('/');
requestedManagerId = requestedManagerId[requestedManagerId.indexOf("team") + 1];

let rosterRequest = new XMLHttpRequest();
rosterRequest.open("GET", "/autumntech/app/getteam/" + requestedManagerId);

rosterRequest.onload = (e) => {
  let roster = JSON.parse(rosterRequest.responseText);

  let teamList = document.getElementById("team_list");
  for(const employee of roster) {
    teamList.insertBefore(createEmployeeElement(employee), null);
  }
};

rosterRequest.send();

function createEmployeeElement(employee) {
  let container = document.createElement("li");
  container.setAttribute("class", "employee_container");

  let link = document.createElement("a");
  link.setAttribute("href", "/autumntech/app/user/" + employee.id + "/");
  link.setAttribute("class", "employee_link");

  let name = document.createElement("h3");
  name.innerText = employee.firstName + " " + employee.lastName;
  let position = document.createElement("h4");
  position.innerText = employee.position;

  link.insertBefore(name, null);
  link.insertBefore(position, null);

  container.insertBefore(link, null);
  return container;
}