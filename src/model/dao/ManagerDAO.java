package model.dao;

import java.util.List;

import model.obj.Employee;
import model.obj.Reimbursement;

/**
 * This class denotes all actions a manager can perform with the database.
 * @author Matt Imel
 */
public interface ManagerDAO {
	/**
	 * Approves a pending reimbursement request.
	 * @param rid The id of the reimbursement request to approve.
	 * @return 
	 */
	public boolean approveReimbursementRequest(int rid);
	
	/**
	 * Denies a pending reimbursement request.
	 * @param rid The id of the reimbursement request to deny.
	 */
	public boolean denyReimbursementRequest(int rid);
	
	/**
	 * Gets all employees that are managed under a particular manager.
	 * @param mid The id of the manager to the employees of.
	 * @return A list of employees under the specified manager, or null if the manager id doesn't exist.
	 */
	public List<Employee> getAllEmployeesUnder(int mid);
	
	/**
	 * Gets all reimbursement requests, pending and resolved, of the employees of a particular manager.
	 * @param mid The id of the manager to query.
	 * @return A list of all reimbursements sent by employees under the given manager.
	 */
	public List<Reimbursement> getAllSubordinateRequests(int mid);
	
	/**
	 * Gets a list of employees that match a given name query.
	 * @param nameQuery A query that may have part of all of an employee's name.
	 * @return A list of all employees that match the given query.
	 */
	public List<Employee> getEmployeesByName(String nameQuery);
	
	/**
	 * Gets a list of reimbursements based on a given title query.
	 * @param titleQuery A query that may have part or all of a reimbursement's name.
	 * @return A list of all reimbursements that match the given query.
	 */
	public List<Reimbursement> getReimbursementsByTitle(String titleQuery);
}
