package model.dao;

import java.util.List;

import model.obj.Employee;
import model.obj.Reimbursement;

/**
 * An employee DAO used in interfacing with the database.
 * @author Matt Imel
 */
public interface EmployeeDAO {
	
	/**
	 * Logs a user in.
	 * @param email The email address, identifying the user to check.
	 * @param password The password to check.
	 * TODO return type will change.
	 * @return 
	 */
	public Employee login(String email, String password);
	
	/**
	 * Registers an employee to the database. This employee can immediately log in and make requests of their station.
	 * @param e The employee to register.
	 */
	public boolean registerEmployee(Employee e);
	
	
	/**
	 * Gets an employee's information based on the id given.
	 * @param eid The employee's id.
	 * @return An Employee object with all relevant information filled, or null if the id does not match any employee in the database.
	 */
	public Employee getEmployee(int eid);
	
	/**
	 * Changes the details of an employee.
	 * @param e The new details of the employee.
	 */
	public boolean updateEmployee(Employee e);
	
	/**
	 * Submits a reimbursement request to the server.
	 * @param r The full reimbursement request.
	 */
	public boolean submitReimbursementRequest(Reimbursement r);
	
	/**
	 * 
	 * @param rid
	 * @return
	 */
	public int getOwnerId(int rid);
	
	/**
	 * Returns a reimbursement with the given id.
	 * @param rid The id of the reimbursement to retrieve.
	 * @return A reimbursement with details of the given id, or null if the id is invalid.
	 */
	public Reimbursement viewOneReimbursementRequest(int rid);
	
	/**
	 * Returns all the reimbursements the mentioned employee has made.
	 * @param eid The id of the employee to check.
	 * @return All reimbursement requests this user has made.
	 */
	public List<Reimbursement> viewAllReimbursementRequests(int eid);
}
