package test.model.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import static org.mockito.ArgumentMatchers.any;

import java.util.Arrays;
import java.util.List;

import model.dao.EmployeeDAO;
import model.dao.impl.EmployeeDAOImpl;
import model.dao.impl.ManagerDAOImpl;
import model.obj.Employee;
import model.obj.Reimbursement;
import model.obj.ReimbursementStatus;
import model.service.EmployeeService;
import model.service.ManagerService;

class ManagerServiceTest {
	@InjectMocks
	private static ManagerService ms;
	
	@Mock
	private EmployeeService es;
	
	@Mock
	private ManagerDAOImpl mdi;
	
	@Mock
	private EmployeeDAOImpl edi;
	
	private static Employee dummyEmployee;
	private static Reimbursement dummyReimbursement;
	private static Reimbursement approvedDummyReimbursement;
	
	@BeforeAll
	public static void init() {
		ms = new ManagerService();
		dummyEmployee = new Employee(
				44,
				"Test",
				"Employee",
				"CEO",
				"test@autech.com",
				"(913)-232-4231",
				"pass",
				"",
				false,
				55
		);
		
		dummyReimbursement = new Reimbursement(
				3,
				3,
				"Test Rbsmt",
				"This is a test reimbursement.",
				"",
				ReimbursementStatus.PENDING,
				30.00
		);
		
		approvedDummyReimbursement = new Reimbursement(
				5,
				3,
				"Test Rbsmt 2",
				"This is a test reimbursement 2.",
				"",
				ReimbursementStatus.APPROVED,
				45.00
		);
	}
	
	@BeforeEach
	public void openMocks() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	void testApproveSuccess() {
		int rid = dummyReimbursement.getId();
		Mockito.when(this.es.viewOneReimbursementRequest(any(Integer.class), any(EmployeeDAO.class))).thenReturn(dummyReimbursement);
		Mockito.when(this.mdi.approveReimbursementRequest(rid)).thenReturn(true);
		
		boolean success = ms.approveReimbursementRequest(rid, mdi, es);
		
		Assertions.assertEquals(true, success);
	}
	
	@Test
	void testApproveFailureApproved() {
		int rid = dummyReimbursement.getId();
		Mockito.when(this.es.viewOneReimbursementRequest(any(Integer.class), any(EmployeeDAO.class))).thenReturn(approvedDummyReimbursement);
		Mockito.when(this.mdi.approveReimbursementRequest(rid)).thenReturn(true);
		
		boolean success = ms.approveReimbursementRequest(rid, mdi, es);
		
		Assertions.assertEquals(false, success);
	}
	
	@Test
	void testApproveFailureNull() {
		int rid = dummyReimbursement.getId();
		Mockito.when(this.es.viewOneReimbursementRequest(any(Integer.class), any(EmployeeDAO.class))).thenReturn(null);
		Mockito.when(this.mdi.approveReimbursementRequest(rid)).thenReturn(true);
		
		boolean success = ms.approveReimbursementRequest(rid, mdi, es);
		
		Assertions.assertEquals(false, success);
	}
	
	@Test
	void testDenySuccess() {
		int rid = dummyReimbursement.getId();
		Mockito.when(this.es.viewOneReimbursementRequest(any(Integer.class), any(EmployeeDAO.class))).thenReturn(dummyReimbursement);
		Mockito.when(this.mdi.denyReimbursementRequest(rid)).thenReturn(true);
		
		boolean success = ms.denyReimbursementRequest(rid, mdi, es);
		
		Assertions.assertEquals(true, success);
	}
	
	@Test
	void testDenyFailureApproved() {
		int rid = dummyReimbursement.getId();
		Mockito.when(this.es.viewOneReimbursementRequest(any(Integer.class), any(EmployeeDAO.class))).thenReturn(approvedDummyReimbursement);
		Mockito.when(this.mdi.denyReimbursementRequest(rid)).thenReturn(true);
		
		boolean success = ms.denyReimbursementRequest(rid, mdi, es);
		
		Assertions.assertEquals(false, success);
	}
	
	@Test
	void testDenyFailureNull() {
		int rid = dummyReimbursement.getId();
		Mockito.when(this.es.viewOneReimbursementRequest(any(Integer.class), any(EmployeeDAO.class))).thenReturn(null);
		Mockito.when(this.mdi.denyReimbursementRequest(rid)).thenReturn(true);
		
		boolean success = ms.denyReimbursementRequest(rid, mdi, es);
		
		Assertions.assertEquals(false, success);
	}
	
	@Test
	void testGetAllSubordinateRequestsSuccess() {
		int mid = 3;
		Mockito.when(this.mdi.getAllSubordinateRequests(mid)).thenReturn(
			Arrays.asList(dummyReimbursement, approvedDummyReimbursement)
		);
		
		List<Reimbursement> rbsmts = ms.getAllSubordinateRequests(mid, mdi);
		
		Assertions.assertEquals(45.00, rbsmts.get(1).getAmount());
	}
	
	@Test
	void testGetAllSubordinatesSuccess() {
		int mid = 55;
		Mockito.when(this.mdi.getAllEmployeesUnder(mid)).thenReturn(Arrays.asList(dummyEmployee));
		
		List<Employee> employees = ms.getAllEmployeesUnder(mid, mdi);
		
		Assertions.assertEquals("CEO", employees.get(0).getPosition());
	}
	
	@Test
	void testSearchEmployeesByNameSuccess() {
		String query = "te";
		Mockito.when(this.mdi.getEmployeesByName(query)).thenReturn(Arrays.asList(dummyEmployee));
	
		List<Employee> results = ms.searchEmployeesByName(query, mdi);
		
		Assertions.assertEquals("CEO", results.get(0).getPosition());
	}
	
	@Test
	void testSearchReimbursementsByTitleSuccess() {
		String query = "rbs";
		Mockito.when(this.mdi.getReimbursementsByTitle(query)).thenReturn(Arrays.asList(dummyReimbursement, approvedDummyReimbursement));
		
		List<Reimbursement> results = ms.searchReimbursementsByTitle(query, mdi);
		
		Assertions.assertEquals("Test Rbsmt 2", results.get(1).getTitle());
	}
}
