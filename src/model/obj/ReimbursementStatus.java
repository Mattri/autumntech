package model.obj;

public enum ReimbursementStatus {
	PENDING,
	APPROVED,
	DENIED
}
