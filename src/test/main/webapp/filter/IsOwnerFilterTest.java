package test.main.webapp.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import main.webapp.IsOwnerFilter;

class IsOwnerFilterTest {
	
	@InjectMocks
	private static IsOwnerFilter iof;
	
	@Mock
	private HttpServletRequest sReq;
	
	@Mock
	private HttpServletResponse sRes;
	
	@Mock
	private HttpSession sess;
	
	@Mock
	private FilterChain fc;
	
	@BeforeAll
	public static void init() {
		iof = new IsOwnerFilter();
	}
	
	@BeforeEach
	public void openMocks() {
		MockitoAnnotations.openMocks(this);
	}


	@Test
	void testIsOwnerSuccess() {
		int ownerId = 4;
		Mockito.when(sReq.getPathInfo()).thenReturn("/" + ownerId);
		Mockito.when(sReq.getSession(false)).thenReturn(sess);
		Mockito.when(sess.getAttribute("id")).thenReturn("" + ownerId);
		
		try {
			iof.doFilter(sReq, sRes, fc);
			Mockito.verify(fc).doFilter(sReq, sRes);
		} catch (IOException | ServletException e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}
	
	@Test
	void testIsOwnerFailure() {
		int ownerId = 4;
		Mockito.when(sReq.getPathInfo()).thenReturn("/" + ownerId);
		Mockito.when(sReq.getSession(false)).thenReturn(sess);
		Mockito.when(sess.getAttribute("id")).thenReturn("" + 5);
		
		try {
			iof.doFilter(sReq, sRes, fc);
			Mockito.verify(sRes).sendRedirect("/autumntech/403.html");
		} catch (IOException | ServletException e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}
}
