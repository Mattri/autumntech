package main.webapp;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet manages all non-parameterized POST requests.
 * @author Matt Imel
 */
@WebServlet("/MainServlet")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String URI = request.getRequestURI();
		MainPostHelperStrategy mphs = null;
		
		switch(URI.substring(URI.lastIndexOf('/') + 1)) {
			case "logout":
				mphs = new LogoutStrategy();
				break;
			case "submit_rbsmt":
				mphs = new ReimbursementSubmissionStrategy();
				break;
			case "edit":
				mphs = new ProfileEditStrategy();
		}
		
		if(mphs != null) {
			mphs.executePost(request, response);
		}
	}

}
