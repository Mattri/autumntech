CREATE TYPE reimbursement_status as ENUM ('pending', 'approved', 'denied');

CREATE TABLE employees (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR NOT NULL,
  last_name VARCHAR NOT NULL,
  "position" VARCHAR NOT NULL,
  email VARCHAR UNIQUE NOT NULL,
  "password" VARCHAR NOT NULL,
  phone VARCHAR NOT NULL,
  avatar_path VARCHAR,
  is_manager BOOLEAN NOT NULL DEFAULT false,
  manager_id INTEGER,
  created_on TIMESTAMP DEFAULT now()
);

CREATE TABLE reimbursements (
  id SERIAL PRIMARY KEY,
  author INTEGER REFERENCES employees(id),
  title VARCHAR NOT NULL,
  "description" VARCHAR,
  amount NUMERIC(11, 2) NOT NULL,
  "status" reimbursement_status NOT NULL DEFAULT 'pending',
  image_path VARCHAR NOT NULL,
  created_on TIMESTAMP DEFAULT now()
);