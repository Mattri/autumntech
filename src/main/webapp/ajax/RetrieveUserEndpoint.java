package main.webapp.ajax;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.webapp.URLChecker;
import model.dao.impl.EmployeeDAOImpl;
import model.obj.Employee;
import model.service.EmployeeService;

/**
 * Servlet implementation class RetrieveUserEndpoint
 */
@WebServlet("/RetrieveUserEndpoint")
public class RetrieveUserEndpoint extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Retrieves information regarding a single user.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int requestedId = 0;
		String path = request.getPathInfo();
		if(!URLChecker.validateSingleParameterPath(path)) {
			return;
		}
		
		try {
			requestedId = Integer.parseInt(path.substring(path.indexOf('/') + 1));
		} catch(NumberFormatException nfe) {
			return;
		}
		
		Employee e = new EmployeeService().getEmployee(requestedId, new EmployeeDAOImpl());
		
		if(e != null) {
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(e.toString());
		}
	}
}
