package model.obj;

/**
 * A reimbursement data object.
 * @author Matt Imel
 */
public class Reimbursement {
	private int id;
	
	private int author;
	
	private String title;
	
	private String description;
	
	private String imageLink;
	
	private ReimbursementStatus status;
	
	private double amount;

	public Reimbursement(int id, int author, String title, String description, String imageLink, ReimbursementStatus status, double amount) {
		this.id = id;
		this.author = author;
		this.title = title;
		this.description = description;
		this.imageLink = imageLink;
		this.status = status;
		this.amount = amount;
	}

	public int getId() {
		return id;
	}
	
	public int getAuthor() {
		return author;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getImageLink() {
		return imageLink;
	}
	
	public ReimbursementStatus getStatus() {
		return status;
	}

	public double getAmount() {
		return amount;
	}
	
	@Override
	public String toString() {
		return "{\"id\":" + id + ", \"author\":" + author + ", \"title\":\"" + title + "\", \"description\":\"" + description
				+ "\", \"imageLink\":\"" + imageLink + "\", \"status\":\"" + status + "\", \"amount\":" + amount + "}";
	}
}
