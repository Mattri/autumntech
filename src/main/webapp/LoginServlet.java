package main.webapp;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.impl.EmployeeDAOImpl;
import model.obj.Employee;
import model.service.EmployeeService;

/**
 * This servlet manages user login.
 * @author Matt Imel
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		final String email = request.getParameter("email");
		final String password = request.getParameter("password");
		
		Employee acct = new EmployeeService().login(email, password, new EmployeeDAOImpl());

		if(acct != null) {
			HttpSession sess = request.getSession();
			sess.setAttribute("id", acct.getId());
			sess.setAttribute("name", acct.getFirstName() + " " + acct.getLastName());
			sess.setAttribute("email", acct.getEmail());
			sess.setAttribute("phone", acct.getPhoneNumber());
			sess.setAttribute("isManager", acct.isManager());
			
			response.sendRedirect("/autumntech/app/");
		} else {
			response.sendRedirect("/autumntech/");
		}
	}
}
